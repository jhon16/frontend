function createPerson(){
  app.factory('createFactory', function ($http) {
    return {
      createPerson: function (personData) {
      return $http.post('http://localhost:3000/people', personData)
      } 
    }
  });
}